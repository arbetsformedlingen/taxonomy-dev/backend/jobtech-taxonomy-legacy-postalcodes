(ns jobtech-taxonomy-legacy-postalcodes.core
  (:require
   [clojure.java.io :as io]
   [clojure.edn :as edn])
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

(defn load-edn
  "Load edn from an io/reader source (filename or io/resource)."
  [source]
  (try
    (with-open [r (io/reader source)]
      (edn/read (java.io.PushbackReader. r)))

    (catch java.io.IOException e
      (printf "Couldn't open '%s': %s\n" source (.getMessage e)))

    (catch RuntimeException e
      (printf "Error parsing edn file '%s': %s\n" source (.getMessage e)))))

(defn load-postalcodes []
  (load-edn (-> "postalcodes.edn"
                io/resource
                io/input-stream))
  )

(def memoized-load-postalcodes (memoize load-postalcodes))


(def postalcodes (memoized-load-postalcodes))
